﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    // For triggering transition
    public GameObject dooroutside;
    public GameObject doorinside;
    public GameObject interactiontext;
    public string sceneName;
    public bool returntooverworld;

    public int LevelToLoad;

    // For entering door
    private bool playerDetected;
    public Transform doorPos;
    public float width;
    public float height;

    public LayerMask whatIsPlayer;

    private void Update()
    {
        // Creating trigger event
        if(playerDetected == true)
        {
            if(Input.GetKeyDown(KeyCode.F))
            {
                if (returntooverworld)
                {
                    PlayerMovement.useplayerprefs = true;
                }
                else
                {
                    PlayerPrefs.SetFloat("PositionX", transform.position.x);
                    PlayerPrefs.SetFloat("PositionY", transform.position.y - 1);
                }
                
                SceneManager.LoadScene(sceneName);
            }
        }
    }

 




    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Collided with: " + collider.transform);

        // Character interacting and entering door
        if (collider.tag == "Player")
        {
            playerDetected = true;
            if (dooroutside)dooroutside.SetActive(false);   
            if (doorinside)doorinside.SetActive(false);
            if (interactiontext)interactiontext.SetActive(true);
        }
        
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        Debug.Log("Collided with: " + collider.transform);

        // Character interacting and exiting door
        if (collider.tag == "Player")
        {
            playerDetected = false;
            if(dooroutside)dooroutside.SetActive(true);
            if(doorinside)doorinside.SetActive(true);
            if (interactiontext)interactiontext.SetActive(false);
        }

    }
}
