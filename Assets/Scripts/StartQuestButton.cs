﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartQuestButton : MonoBehaviour
{
    // For triggering transition
    public GameObject interactiontext;
    public string questFilePath;
    public QuestManager questManager;
    public GameObject QuestMan;

    // For entering door
    private bool playerDetected;
    public Transform doorPos;
    public float width;
    public float height;
    public GameObject dooroutside;
    public GameObject doorinside;
    public string sceneName;
    public bool returntooverworld;
    public GameObject Dialog;

    public LayerMask whatIsPlayer;

    public static int banditsKilled;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            banditsKilled = 5;
        }

        // Creating trigger event
        if (playerDetected == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                questManager.StartQuest(questFilePath);
                if (banditsKilled >= 5)
                {
                    FindObjectOfType<QuestDisplayComponent>().ProgressNode();
                    questManager.EndQuest(questFilePath);
                }
            }
        }
    }
    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Collided with: " + collider.transform);

        // Character interacting and entering door
        /*if (Input.GetKeyDown(KeyCode.E))
        {
            questManager.StartQuest(questFilePath);
        }*/
        playerDetected = true;

    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        Debug.Log("Collided with: " + collider.transform);

        // Character interacting and exiting door
        if (collider.tag == "Player")
        {
            Dialog.SetActive(false);
        }

    }
}
