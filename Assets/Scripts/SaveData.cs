﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using System.Numerics;

[System.Serializable]
public class SaveData
{
    public string sceneName;
    public float[] playerPosition;
}

public class SaveSystem
{
    public static string sceneName;
    public static UnityEngine.Vector3 playerPosition;

    public static void SaveToFile()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/save.mildwest";

        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, ConvertToData());

        stream.Close();
    }
    static SaveData ConvertToData()
    {
        SaveData data = new SaveData();

        data.sceneName = sceneName;

        data.playerPosition = new float[3];
        data.playerPosition[0] = playerPosition.x;
        data.playerPosition[1] = playerPosition.y;
        data.playerPosition[2] = playerPosition.z;

        return data;
    }

    public static void LoadFromFile()
    {
        string path = Application.persistentDataPath + "/save.mildwest";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            FileStream stream = new FileStream(path, FileMode.Open);

            ConvertFromData(formatter, stream);
            stream.Close();
        }
        else
        {
            
        }
    }

    static void ConvertFromData(BinaryFormatter formatter, FileStream stream)
    {
        SaveData data = formatter.Deserialize(stream) as SaveData;

        sceneName = data.sceneName;

        playerPosition.x = data.playerPosition[0];
        playerPosition.y = data.playerPosition[1];
        playerPosition.z = data.playerPosition[2];
    }

   
}
