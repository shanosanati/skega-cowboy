﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RandomEncounter : MonoBehaviour
{
   
    public GameObject Player;
    public float encounterChance;
    public string sceneName;

    float timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.D)))))
        {
            float randomPercent = Random.Range(0, 100f);
            timer += Time.deltaTime;
            if (randomPercent < encounterChance)
            {
                //Start Encounter
                SceneManager.LoadScene (sceneName);
            }
        }
    }
}
