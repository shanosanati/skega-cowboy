﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Shop : MonoBehaviour
{
    private bool playerDetected;

    public GameObject shopItemTemplate;

    public LayerMask whatIsPlayer;

    public static int healthItems;

    private void Start()
    {
        shopItemTemplate.SetActive(false);

    }

    private void Update()
    {
        // Creating trigger event
        if (playerDetected == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
               if (PlayerStats.Gold >= 5)
                {
                    UI_Shop.healthItems++;
                }
            
            }

        }
    }
    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Collided with: " + collider.transform);

        playerDetected = true;
        shopItemTemplate.SetActive(true);


    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        Debug.Log("Collided with: " + collider.transform);

        // Character interacting and exiting door
        if (collider.tag == "Player")
        {
            playerDetected = false;

            shopItemTemplate.SetActive(false);
        }

    }
}
