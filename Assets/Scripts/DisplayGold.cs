﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayGold : MonoBehaviour
{

    Text txt;

    // Use this for initialization
    void Start()
    {
        txt = gameObject.GetComponent<Text>();
        txt.text = "Purse : " + PlayerStats.Gold;
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = "Purse : " + PlayerStats.Gold;
    }
}