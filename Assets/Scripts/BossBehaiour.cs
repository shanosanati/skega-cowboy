﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossBehaiour : MonoBehaviour
{
    public BattleSystem battleSystem;
    Unit unit;
    public int currentHealth;
    public int maximumHealth;
    public int minimumHealth;
    public int healAmount;
    private bool damaged;
    private bool healing;



    // Start is called before the first frame update
    void Start()
    {
        unit = GetComponent<Unit>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!healing && unit.currentHP < 25)
        {
            Heal(healAmount);
        }

        // Creating trigger event
        if (playerDetected == true)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (returntooverworld)
                {
                    PlayerMovement.useplayerprefs = true;
                }
                else
                {
                    PlayerPrefs.SetFloat("PositionX", transform.position.x);
                    PlayerPrefs.SetFloat("PositionY", transform.position.y - 1);
                }

                SceneManager.LoadScene(sceneName);
            }
        }
    }

    public void TakeDamage(int damage)
    {
        damaged = true;
        if (currentHealth > minimumHealth)
        {
            currentHealth = currentHealth - damage;
        }
    }

    public void Heal(int healAmount)
    {
        healing = true;

        unit.currentHP = unit.currentHP + healAmount;
        
    }



    public string sceneName;
    public bool returntooverworld;

    public int LevelToLoad;

    // For entering door
    private bool playerDetected;
    public float width;
    public float height;

    public LayerMask whatIsPlayer;


    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Collided with: " + collider.transform);

        // Character interacting and entering door
        if (collider.tag == "Player")
        {
            playerDetected = true;
            SceneManager.LoadScene(sceneName);
        }
    }
}
