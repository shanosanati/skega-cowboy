﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : MonoBehaviour
{
    public string questFilePath;
    QuestManager questManager;
    QuestDisplayComponent questDisplayComponent;
    // Start is called before the first frame update
    void Start()
    {
        questManager = FindObjectOfType<QuestManager>();
        questDisplayComponent = FindObjectOfType<QuestDisplayComponent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (questDisplayComponent == null)
        {
            questDisplayComponent = FindObjectOfType<QuestDisplayComponent>();
        }
   
    
        if (questDisplayComponent.Quest != null && !questDisplayComponent.Quest.HasNextQuestNode())
        {
       
        }
    }

    
}
