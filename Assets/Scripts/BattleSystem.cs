﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOST }

public class BattleSystem : MonoBehaviour
{

    public GameObject playerPrefab;
    public GameObject enemyPrefab;

    public Transform playerBattleStation;
    public Transform enemyBattleStation;

    Unit playerUnit;
    Unit enemyUnit;
    Unit unit;

    public Text dialogueText;

    public BattleHUD playerHUD;
    public BattleHUD enemyHUD;
     
    public BattleState state;

    public int healAmount;
    public bool healing;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleState.START;
        StartCoroutine(SetupBattle());
        unit = GetComponent<Unit>();
    }

    // Calls integers from units script to setup the battle
    IEnumerator SetupBattle()
    {
        GameObject playerGO = Instantiate(playerPrefab, playerBattleStation);
        playerUnit = playerGO.GetComponent<Unit>();

        GameObject enemyGO = Instantiate(enemyPrefab, enemyBattleStation);
        enemyUnit = enemyGO.GetComponent<Unit>();

        dialogueText.text = "A wild " + enemyUnit.unitName + " approaches...";

        // Calls from BattleHUD
        playerHUD.SetHUD(playerUnit);
        enemyHUD.SetHUD(enemyUnit);

        yield return new WaitForSeconds(2f);

        state = BattleState.PLAYERTURN;
        PlayerTurn();
    }

    // Uses variables from the unit script to attack the enemy
    IEnumerator PlayerAttack()
    {
        bool isDead = enemyUnit.TakeDamage(playerUnit.damage);

        enemyHUD.SetHP(enemyUnit.currentHP);
        dialogueText.text = "The attack is sucessful!";

        yield return new WaitForSeconds(1f);

        if(isDead)
        {
            state = BattleState.WON;
            EndBattle();
        } else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        
    }


    // Uses a special attack to deal more damage
    IEnumerator PlayerSpecialAttack()
    {
        bool isDead = enemyUnit.TakeDamage(playerUnit.specialdamage);

        enemyHUD.SetHP(enemyUnit.currentHP);
        dialogueText.text = "The attack is sucessful!";

        yield return new WaitForSeconds(1f);

        if (isDead)
        {
            state = BattleState.WON;
            EndBattle();
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }

    }

    public void Heal(int healAmount)
    {
        healing = true;

        playerUnit.currentHP = playerUnit.currentHP + healAmount;
        playerHUD.SetHP(playerUnit.currentHP);
    }

    // Uses variables from the unit script to attack the player
    IEnumerator EnemyTurn()
    {
        dialogueText.text = enemyUnit.unitName + " attacks!";

        yield return new WaitForSeconds(1f);

        bool isDead = playerUnit.TakeDamage(enemyUnit.damage);

        playerHUD.SetHP(playerUnit.currentHP);

        yield return new WaitForSeconds(1f);

        if(isDead)
        {
            state = BattleState.LOST;
            EndBattle();
        } else 
        {
            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
    }


    // Prints if the player won the battle or not and returns the player to either Hub World or Encounter Area
    void EndBattle()
    {
        if (state == BattleState.WON)
        {
            
            if (enemyUnit.GetComponent<BossBehaiour>())
            {
                dialogueText.text = "You won the game!";

            }
            else
            {
                dialogueText.text = "You won the battle!";
            }
            PlayerStats.Gold += 2;
            StartQuestButton.banditsKilled++;

            if (enemyUnit.GetComponent<BossBehaiour>())
            {
                Invoke("LoadYouWin", 3);

            }
            else
            {
                Invoke("LoadEncounterArea", 3);
            }


        } else if (state == BattleState.LOST)
        {
            dialogueText.text = "You were defeated";
            PlayerStats.Health = 0;

                Invoke("LoadYouLose", 3);
;
        }
    }

    // Called in the End Battle code
    void LoadEncounterArea()
    {
        SceneManager.LoadScene("Encounter Area");
    }

    // Called in the End Battle code
    void LoadHubWorld()
    {
        SceneManager.LoadScene("Hub World");
    }
    

    void PlayerTurn()
    {
        dialogueText.text = "Choose an action:";
    }

    // Sets up Attack Button
    public void OnAttackButton()
    {
        if (state != BattleState.PLAYERTURN) 
            return;

        StartCoroutine(PlayerAttack());
    }

    // Sets up Special Attack Button
    public void OnSpecialAttackButton()
    {
        if (state != BattleState.PLAYERTURN)
            return;

        if (playerUnit.specialattackLimit <= 0)
            return;
        StartCoroutine(PlayerSpecialAttack());
        playerUnit.specialattackLimit -= 1;
    }

    // Sets up Item button
    public void ItemButton()
    {
        if (state != BattleState.PLAYERTURN)
            return;

        Heal(playerUnit.healAmount);
    }


    // Sets up Run button
    public void RunButton()
    {
        SceneManager.LoadScene("Encounter Area");
    }


}
