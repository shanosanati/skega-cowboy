﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningBoss : MonoBehaviour
{
    public GameObject Boss;
    public GameObject BoundaryLeft;
    public GameObject BoundaryRight;
    public GameObject BoundaryBottom;
    
    // Start is called before the first frame update
    void Start()
    {
        Boss.SetActive(false);
        Boss.GetComponent<BoxCollider2D>().enabled = false;
        BoundaryLeft.GetComponent<BoxCollider2D>().enabled = false;
        BoundaryRight.GetComponent<BoxCollider2D>().enabled = false;
        BoundaryBottom.GetComponent<BoxCollider2D>().enabled = false;
        if (PlayerPrefs.GetInt("SpawnBoss") == 1)
        {
            PlayerPrefs.SetInt("SpawnBoss", 2);
            SpawnBoss();
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    void SpawnBoss()
    {
        Boss.SetActive(true);
        Boss.GetComponent<BoxCollider2D>().enabled = true;
        BoundaryLeft.GetComponent<BoxCollider2D>().enabled = true;
        BoundaryRight.GetComponent<BoxCollider2D>().enabled = true;
        BoundaryBottom.GetComponent<BoxCollider2D>().enabled = true;

    }
}
